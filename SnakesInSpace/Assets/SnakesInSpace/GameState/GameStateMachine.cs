﻿using System;
using SnakesInSpace.Input;
using Stateless;
using UnityEngine;
using Zenject;

namespace SnakesInSpace.GameState
{
    public class GameStateMachine : MonoBehaviour
    {
        public enum GameState
        {
            Startup,
            MainMenu,
            Gameplay
        }
        
        public enum GameStateTrigger
        {
            Initialize,
            StartGame,
            CompleteGame
        }

        private InputHandler _inputHandler;
        
        private StateMachine<GameState, GameStateTrigger> _stateMachine;
        private GameState _currentState;

        [Inject]
        public void Constructor(InputHandler inputHandler)
        {
            _inputHandler = inputHandler;
        }
        
        // Start is called before the first frame update
        private void Start()
        {
            _stateMachine = new StateMachine<GameState, GameStateTrigger>(
                () => _currentState, 
                s => _currentState = s);
            
            _stateMachine.Configure(GameState.Startup)
                .Permit(GameStateTrigger.Initialize, GameState.MainMenu);
            _stateMachine.Configure(GameState.MainMenu)
                .OnEntry(OnMainMenuEntry)
                .OnExit(OnMainMenuExit)
                .Permit(GameStateTrigger.StartGame, GameState.Gameplay);
            _stateMachine.Configure(GameState.Gameplay)
                .OnEntry(OnGameplayEntry)
                .OnExit(OnGameplayExit)
                .Permit(GameStateTrigger.CompleteGame, GameState.MainMenu);

        }

        private void Update()
        {
            switch (_currentState)
            {
                case GameState.Startup:
                    break;
                case GameState.MainMenu:
                    if (_inputHandler.StartGame)
                    {
                        _stateMachine.Fire(GameStateTrigger.StartGame);
                    }
                    break;
                case GameState.Gameplay:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void OnMainMenuEntry(StateMachine<GameState, GameStateTrigger>.Transition obj)
        {
            // Transition UI here.
        }

        private void OnMainMenuExit(StateMachine<GameState, GameStateTrigger>.Transition obj)
        {
            // Transition away ui here.
        }

        private void OnGameplayEntry(StateMachine<GameState, GameStateTrigger>.Transition obj)
        {
            // Start logic for game
        }

        private void OnGameplayExit(StateMachine<GameState, GameStateTrigger>.Transition obj)
        {
            // End logic for game.
        }
    }
}
