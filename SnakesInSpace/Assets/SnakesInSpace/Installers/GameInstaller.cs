using SnakesInSpace.GameState;
using SnakesInSpace.Input;
using Zenject;

namespace SnakesInSpace.Installers
{
    public class GameInstaller : MonoInstaller<GameInstaller>
    {
        public override void InstallBindings()
        {
            // Input
            Container.Bind<InputHandler>().FromNewComponentOn(gameObject).AsSingle();
            
            // State machine
            Container.Bind<GameStateMachine>().FromNewComponentOn(gameObject).AsSingle().NonLazy();
        }
    }
}