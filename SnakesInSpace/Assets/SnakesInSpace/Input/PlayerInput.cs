namespace SnakesInSpace.Input
{
    public class PlayerInput
    {
        public enum PlayerDirection
        {
            Up, Down, Left, Right
        }
        
        public PlayerDirection CurrentDirection { get; set; }
        public bool UsePower { get; set; }
    }
}