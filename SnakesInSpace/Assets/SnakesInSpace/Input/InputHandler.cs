using UnityEngine;

namespace SnakesInSpace.Input
{
    public class InputHandler : MonoBehaviour
    {
        public bool StartGame;
        
        public PlayerInput Player1, Player2;

        public void Udpate()
        {
            StartGame = UnityEngine.Input.GetButton("StartGame");
            
            if (UnityEngine.Input.GetButton("Player1Up"))
            {
                Player1.CurrentDirection = PlayerInput.PlayerDirection.Up;
            }
            if (UnityEngine.Input.GetButton("Player1Down"))
            {
                Player1.CurrentDirection = PlayerInput.PlayerDirection.Down;
            }
            if (UnityEngine.Input.GetButton("Player1Left"))
            {
                Player1.CurrentDirection = PlayerInput.PlayerDirection.Left;
            }
            if (UnityEngine.Input.GetButton("Player1Right"))
            {
                Player1.CurrentDirection = PlayerInput.PlayerDirection.Right;
            }

            Player1.UsePower = UnityEngine.Input.GetButton("Player1Use");
            
            if (UnityEngine.Input.GetButton("Player2Up"))
            {
                Player2.CurrentDirection = PlayerInput.PlayerDirection.Up;
            }
            if (UnityEngine.Input.GetButton("Player2Down"))
            {
                Player2.CurrentDirection = PlayerInput.PlayerDirection.Down;
            }
            if (UnityEngine.Input.GetButton("Player2Left"))
            {
                Player2.CurrentDirection = PlayerInput.PlayerDirection.Left;
            }
            if (UnityEngine.Input.GetButton("Player2Right"))
            {
                Player1.CurrentDirection = PlayerInput.PlayerDirection.Right;
            }
            
            Player2.UsePower = UnityEngine.Input.GetButton("Player2Use");
        }
    }
}